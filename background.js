chrome.action.onClicked.addListener(() => {
  sortBookmarks();
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === 'sortBookmarks') {
    sortBookmarks();
  }
});


async function sortBookmarks() {
  const bookmarks = await chrome.bookmarks.getTree();
  const domains = {};
  collectDomains(bookmarks[0], domains);

  const sortedFolder = await createSortedFolder();
  await createDomainFolders(sortedFolder.id, domains);

  await moveBookmarks(bookmarks[0], domains, sortedFolder.id);

  chrome.notifications.create({
    type: 'basic',
    iconUrl: 'icons/icon48.png',
    title: 'Bookmark Sorter',
    message: 'Done sorting bookmarks!'
  });
}

function collectDomains(bookmarkNode, domains) {
  if (bookmarkNode.url) {
    const url = new URL(bookmarkNode.url);
    const domain = url.hostname.split('.').slice(-2).join('.');
    if (!domains[domain]) {
      domains[domain] = [];
    }
    domains[domain].push(bookmarkNode.id);
  }

  if (bookmarkNode.children) {
    for (const child of bookmarkNode.children) {
      collectDomains(child, domains);
    }
  }
}

function createSortedFolder() {
  return new Promise((resolve, reject) => {
    chrome.bookmarks.create({ title: 'Sorted' }, folder => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError);
      } else {
        resolve(folder);
      }
    });
  });
}

function createDomainFolders(parentId, domains) {
  const promises = [];
  for (const domain in domains) {
    promises.push(
      new Promise((resolve, reject) => {
        chrome.bookmarks.create({ parentId, title: domain }, folder => {
          if (chrome.runtime.lastError) {
            reject(chrome.runtime.lastError);
          } else {
            domains[domain] = { folderId: folder.id, bookmarks: domains[domain] };
            resolve();
          }
        });
      })
    );
  }
  return Promise.all(promises);
}

function moveBookmarks(bookmarkNode, domains, sortedFolderId) {
  if (bookmarkNode.url) {
    const url = new URL(bookmarkNode.url);
    const domain = url.hostname.split('.').slice(-2).join('.');
    if (domains[domain]) {
      chrome.bookmarks.move(bookmarkNode.id, { parentId: domains[domain].folderId });
    }
  }

  if (bookmarkNode.children) {
    for (const child of bookmarkNode.children) {
      moveBookmarks(child, domains, sortedFolderId);
    }
  }
}
